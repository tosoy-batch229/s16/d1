console.log("Hello World!");

// Arithmetic Operators
let x = 1397;
let y = 7831;

// Addition
let sum = x + y;
console.log("Result of addition operator: " + sum);

// Subtraction
let difference = x - y;
console.log("Result of substraction operator: " + difference);

// Multiplication
let product = x * y;
console.log("Result of multiplication operator: " + product);

// Division
let qoutient = x - y;
console.log("Result of division operator: " + qoutient);

// Modulus
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);


// Assignment Operator 
let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Shorthand Method for assignment operator
/* 
    variable += value;
    variable -= value;
    variable *= value;
    variable /= value;
    variable %= value;
*/

// Multiple Operators & Parentheses

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(`Result of MDAS operation: ${mdas}`);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(`Result of PEMDAS operation: ${pemdas}`);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log(`Result of the second PEMDAS operation: ${pemdas}`);


// Incrementaion vs Decrementation
// Incrementaion (++)
// Decrementation (--)

let z = 1;
let increment = ++z;

console.log(`Result of pre incrementation: ${increment}`);
console.log(`Result of pre incrementation: ${z}`);

increment = z++;
console.log(`Result of pre incrementation: ${increment}`);
console.log(`Result of pre incrementation: ${z}`);

let decrement = --z;
console.log(`Result of post decrementation: ${decrement}`);
console.log(`Result of post decrementation: ${z}`);

decrement = z--;
console.log(`Result of post decrementation: ${decrement}`);
console.log(`Result of post decrementation: ${z}`);


// Type Coercion
let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

let numE = false + 1;
console.log(numE);

let numF = true + 1;
console.log(numF);


// Comparison Operators
let juan = "juan";

// Equality Operator (==) - checks two operands if they are equal or have the same content and return boolean value.
console.log("10" == 10);
console.log(1 == 1);
console.log(1 == 2);
console.log("juan" == "juan");
console.log("juan" == juan);

// Inequality Operator (!=)
console.log("10" != 10);
console.log(1 != 1);
console.log(1 != "1");
console.log(1 != 2);
console.log("juan" != "juan");
console.log("juan" != juan);
console.log(0 != false);

// Strict Equality Checker (===)
console.log(1 === 1);
console.log(1 === 2);
console.log("juan" === juan);

// Strict Inequality (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log("juan" !== juan);


// Relational Operator
let a = 50;
let b = 65;

// Greater than (>)
let isGreaterThan = a > b;
console.log(isGreaterThan);

// Less than (<)
let isLessThan = a < b;
console.log(isLessThan);

// Greater than or equal to (>=)
let isGTorEqual = a >= b;
console.log(isGTorEqual);

// Less than or equal to (<=)
let isLTorEqual = a <= b;
console.log(isLTorEqual);

let numStr = "30"
console.log(a > numStr);

let str = "twenty"
console.log(b > str);

// In some events, we can recieve Nan (Not a Number)

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&&)
let allRequirementsMet = isLegalAge && isRegistered;
console.log(`Result of logical AND operator: ${allRequirementsMet}`);

// Logical || Operator (&&)
let someRequirementsMet = isLegalAge || isRegistered;
console.log(`Result of logical OR operator: ${someRequirementsMet}`);

// Logical NOT Operator (&&)
console.log(!isRegistered);